<?php

namespace Application\Form;

use Zend\Form\Form;                

class Login extends Form
{
	public function __construct()
	{
		parent::__construct('Movie');
		$this->add(array('name'=>'email','type'=>'text',
				'options'=>array('label'=>'Employee Email :'),
                                'attributes'=>array('id'=>'email')));
		$this->add(array('name'=>'password','type'=>'password',
				'options'=>array('label'=>'Employee Password :'),
                                'attributes'=>array('id'=>'password')));
		$this->add(array('name'=>'submit','type'=>'submit',
                               'attributes'=>array('value'=>'Login','id'=>'btn')));
	}
}
